#pragma once

#include <vector>
#include <string>
#include "stdafx.h"

extern int pid ;
extern HANDLE hMapFile  ;
extern LPCTSTR pBuf  ;
extern PositionDataType positiondata[MAX_INSUTRUMENT];

class CShareMemoryPosition
{
public:
	PositionDataType positiondata[MAX_INSUTRUMENT] = { 0 };

	std::map< string, volatile int> map_position;

	double UpdatePosition(string instrumentKey, int i, int id);

	void   *GetPosition(int id);

	int GetPositionNum();

	double InitPositionData(char *InstrumentID);

};
